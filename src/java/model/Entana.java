/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annot.MethAnnotation;
import util.ModelView;

/**
 *
 * @author Best
 */
public class Entana {
    
    private double isa;
    private String anarana;
    
    public double getisa()
    {
        return this.isa;
    }
    public String getanarana()
    {
        return this.anarana;
    }
    
    /**
     *
     * @param isa
     */
    public void setisa(double isa)
    {
        this.isa=isa;
    }
    public void setanarana(String isa)
    {
        this.anarana=isa;
    }
    
    @MethAnnotation(methname="/setentana")
    public ModelView getentana()
    {
        System.out.println("niantso methode getentana");
        ModelView mv=new ModelView();
        mv.seturl("entanairay.jsp");
        mv.getdata().put("entana",this);
        return mv;
    }
    
    @MethAnnotation(methname="/manovaentana")
    public ModelView setentana()
    {
        System.out.println("niantso methode setentana");
        ModelView mv=new ModelView();
        return mv;
    }
}
